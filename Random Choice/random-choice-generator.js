$(function() {

	$("#tf-submit").on("click",function() {
		var allText = $('#oldText').val();
		allText = $.trim(allText);
		var theSep = $("#sep").val();
		if(theSep == "lines"){
			theSep='\n';
		}else if(theSep == "commas"){
			theSep=',';
		}else if(theSep == "semi-colons"){
			theSep=';';
		}else if(theSep == "space"){
			theSep=' ';
		}
		var choiceArray = allText.split(theSep);
		var numOfChoices = (choiceArray.length);
	    var x=Math.floor(Math.random()*numOfChoices);
		$('#newText').val(choiceArray[x]);
	});
	
	$("#tf-reset").on("click",function() {
		$('#oldText').val('')
		$('#newText').val('');
	});
	
});